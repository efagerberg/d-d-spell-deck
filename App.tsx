import * as firebase from 'firebase';
import React, { Component } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  createAppContainer,
  createStackNavigator,
} from 'react-navigation';
import { AuthBar } from './components/AuthBar/AuthBar';
import { AuthLoadingScreen } from './components/AuthLoadingScreen/AuthLoadingScreen.component';
import { TabBar } from './components/TabBar/TabBar';

EStyleSheet.build();

const config = {
  apiKey: 'AIzaSyCX0gTT3n434mGxKVNJ7oGYL-CX8v7vbrE',
  authDomain: 'd-d-spelldeck.firebaseapp.com',
  databaseURL: 'https://d-d-spelldeck.firebaseio.com',
  projectId: 'd-d-spelldeck',
  storageBucket: 'd-d-spelldeck.appspot.com',
  messagingSenderId: '280286561153',
};
firebase.initializeApp(config);

const AppContainer = createAppContainer(
  createStackNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: TabBar,
      Auth: AuthBar,
    },
    {
      initialRouteName: 'AuthLoading',
      cardStyle: {
        backgroundColor: 'black',
        marginTop: 22,
      },
      headerMode: 'none',
    },
  ),
);

export default class App extends Component {
  public render() {
    return <AppContainer />;
  }
}
