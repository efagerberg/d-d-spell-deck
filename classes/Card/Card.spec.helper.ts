import { Card } from './Card';

const defaultCard = {
  title: 'Test Spell',
  level: 1,
  school: 'Evocation',
  castingTime: '1 action',
  range: '40 feet',
  components: 'V,S,M',
  duration: '1 minute',
  // tslint:disable:max-line-length
  description: `Lorem ipsum dolor sit amet, eu sea dolorum posidonium. Est ut bonorum commune, usu falli invidunt no.In vim vero principes, ad pri sanctus vivendum. Ad vel modo deleniti, quo ut adhuc incorrupte. Vis an impetus salutatus, regione urbanitas expetendis in sit. Ex nobis blandit eum, dolore nostrum vel ei.In eam tempor detraxit, adhuc dissentiet id mel.Has habeo noster dicunt et, dicit referrentur pri in, veniam postulant accommodare ad per.Cu sed eros dissentias dissentiet.Mei te singulis instructior, illum vivendum constituam ad eum, porro sadipscing qui at.His vivendo dignissim at, affert percipit has no.`,
  series: 'Monk',
} as Card;

export function generateCard(values = {}): Card {
  return {...defaultCard, ...values} as Card;
}
