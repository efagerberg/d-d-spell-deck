export interface Card {
  title: string;
  level: number;
  school: string;
  castingTime: string;
  range: string;
  components: string;
  duration: string;
  description: string;
  atHigherLevelsDescription?: string;
  series: string;
}
