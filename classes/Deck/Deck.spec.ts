import { Card } from '../Card/Card';
import { generateCard } from '../Card/Card.spec.helper';
import { Deck } from './Deck';

describe('Deck', () => {
  let deck: Deck;
  let cards: Card[];

  beforeEach(() => {
    cards = [
      generateCard({title: 'Test Spell 1'}),
      generateCard({title: 'Test Spell 2'}),
      generateCard({title: 'Test Spell 3'}),
    ];
    deck = new Deck('A test deck', cards);
  });

  it('sets the deck name', () => {
    expect(deck.name).toBe('A test deck');
  });

  describe('.next', () => {
    let previous: Card;
    let expectedNext: Card;
    let next: Card;

    beforeEach(() => {
      previous = deck.first;
      expectedNext = deck[1];
      next = deck.next();
    });

    it('gets the next card in the deck', () => {
      expect(next).toBe(expectedNext);
    });

    it('puts previous card in the back of the deck', () => {
      expect(deck[deck.length - 1]).toBe(previous);
    });
  });

  describe('.previous()', () => {
    let previous: Card;
    let expectedNext: Card;
    let next: Card;

    beforeEach(() => {
      previous = deck.first;
      expectedNext = deck[deck.length - 1];
      next = deck.previous();
    });

    it('gets the last card in the deck', () => {
      expect(next).toBe(expectedNext);
    });

    it('puts previous card at index 1', () => {
      expect(deck[1]).toBe(previous);
    });
  });

  describe('.previous + .next', () => {
    it('has the original order after calling .next then .previous', () => {
      deck.next();
      deck.previous();
      // For some reason expect equals does not work even when deconstructing
      expect(deck[0]).toEqual(cards[0]);
      expect(deck[1]).toEqual(cards[1]);
      expect(deck[2]).toEqual(cards[2]);
    });

    it('has the original order after calling .previous then .next', () => {
      deck.previous();
      deck.next();
      // For some reason expect equals does not work even when deconstructing
      expect(deck[0]).toEqual(cards[0]);
      expect(deck[1]).toEqual(cards[1]);
      expect(deck[2]).toEqual(cards[2]);
    });
  });

  describe('second', () => {
    it('returns the second card of a deck', () => {
      const second = deck[1];
      expect(second).toEqual(deck.second);
    });
  });

  describe('.last', () => {
    it('returns the last card of a deck', () => {
      const last = deck[deck.length - 1];
      expect(last).toEqual(deck.last);
    });

    it('returns null when there is no second card', () => {
      deck = new Deck('A Test Deck', [cards[0]]);
      expect(null).toEqual(deck.second);
    });
  });
});
