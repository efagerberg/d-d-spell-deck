/*
Contains Card instances, and has the ability to
loop around for infinite scrolling.
*/
import { Card } from '../Card/Card';

export class Deck extends Array<Card> {

  public name: string;

  // Needed to correctly extend Array class
  constructor(name: string, items: Card[]) {
    super(...items);
    Object.setPrototypeOf(this, Deck.prototype);
    this.name = name;
  }

  get first(): Card {
    return this[0];
  }

  get second(): Card | null {
    return this.length > 1 ? this[1] : null;
  }

  get last(): Card {
    return this[this.length - 1];
  }

  public next(): Card {
    const prevCurrent = this.shift() as Card;
    this.push(prevCurrent);
    return this.first;
  }

  public previous(): Card {
    const prevCurrent = this.pop() as Card;
    this.unshift(prevCurrent);
    return this.first;
  }
}
