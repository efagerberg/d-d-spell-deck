import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { styles } from './AddButton.styles';

export class AddButton extends Component {
  public render() {
    return (
      <TouchableOpacity>
        <Ionicons
          color='tomato'
          size={70}
          name='md-add-circle'
          style={styles.root} />
      </TouchableOpacity>
    );
  }
}
