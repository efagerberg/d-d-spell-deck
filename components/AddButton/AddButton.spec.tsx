import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { AddButton } from './AddButton.component';

describe('AddButton', () => {
  let wrapper: ShallowWrapper;
  let described: AddButton;

  beforeEach(() => {
    wrapper = shallow(<AddButton/>);
    described = wrapper.instance() as AddButton;
  });

  describe('.render', () => {
    it('renders a touchable opacity that hosts the icon', () => {
      expect(wrapper.find(TouchableOpacity)).toBeDefined();
    });
  });
});
