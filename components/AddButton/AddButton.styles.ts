import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    textAlign: 'right',
    padding: 5,
  },
});
