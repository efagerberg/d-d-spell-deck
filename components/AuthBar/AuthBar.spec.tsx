import React from 'react';
import { Text } from 'react-native';
import { AuthBar, getTabLabel } from './AuthBar';

describe('AuthBar', () => {
  it('should be defined', () => {
    expect(AuthBar).toBeDefined();
  });
});

describe('getTabLabel', () => {
  it('returns a function that returns the label', () => {
    const label = 'Signin';
    expect(getTabLabel(label)({})).toEqual(
      <Text style={{ fontSize: 20 }}>{label}</Text>,
    );
  });
});
