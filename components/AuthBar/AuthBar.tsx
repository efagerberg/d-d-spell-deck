import React from 'react';
import { Text } from 'react-native';
import { createBottomTabNavigator, NavigationRouteConfigMap } from 'react-navigation';
import { SignInScreen } from '../SignInScreen/SignInScreen.component';
import { SignUpScreen } from '../SignUpScreen/SignUpScreen.component';

export function getTabLabel(label: string) {
  return ({}: NavigationRouteConfigMap) => (
    <Text style={{ fontSize: 20 }}>{label}</Text>
  );
}

export const AuthBar = createBottomTabNavigator(
  {
    SignIn: {
      screen: SignInScreen,
      navigationOptions: {
        tabBarIcon: getTabLabel('Sign In'),
      },
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: {
        tabBarIcon: getTabLabel('Sign Up'),
      },
    },
  },
  {
    initialRouteName: 'SignIn',
    tabBarOptions: {
      activeBackgroundColor: 'tomato',
      inactiveBackgroundColor: 'gray',
      activeTintColor: 'black',
      inactiveTintColor: 'black',
      showLabel: false,
    },
  },
);
