import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './AuthLoadingScreen.styles';

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

export class AuthLoadingScreen extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.bootstrapAsync();
  }

  public render() {
    return (
      <View style={styles.root}>
        <ActivityIndicator />
        <StatusBar barStyle='default' />
      </View>
    );
  }

  private bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  }
}
