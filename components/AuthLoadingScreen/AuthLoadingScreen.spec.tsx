import { shallow, ShallowWrapper } from 'enzyme';

import React from 'react';
import { ActivityIndicator, AsyncStorage, StatusBar, View } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { AuthLoadingScreen } from './AuthLoadingScreen.component';

describe('AuthLoadingScreen', () => {
  let wrapper: ShallowWrapper;
  let instance: AuthLoadingScreen;
  let getPromise: Promise<any>;
  let navigation: NavigationScreenProp<any, any>;

  beforeEach(() => {
    navigation = {
      state: {},
      dispatch: jest.fn(),
      goBack: jest.fn(),
      dismiss: jest.fn(),
      openDrawer: jest.fn(),
      closeDrawer: jest.fn(),
      toggleDrawer: jest.fn(),
      getParam: jest.fn(),
      setParams: jest.fn(),
      addListener: jest.fn(),
      push: jest.fn(),
      replace: jest.fn(),
      pop: jest.fn(),
      popToTop: jest.fn(),
      isFocused: jest.fn(),
      navigate: jest.fn(),
      dangerouslyGetParent: jest.fn(),
    };
    wrapper = shallow(<AuthLoadingScreen navigation={navigation} />);
    instance = wrapper.instance() as AuthLoadingScreen;
  });

  describe('when storage has the userToken', () => {
    beforeEach(() => {
      getPromise = new Promise((resolve) => {
        resolve({ userToken: 'foo' });
      });
      AsyncStorage.getItem = jest.fn().mockReturnValueOnce(
        getPromise,
      );
      wrapper = shallow(<AuthLoadingScreen navigation={navigation} />);
      instance = wrapper.instance() as AuthLoadingScreen;
    });

    it('navigates to the App', () => {
      expect(AsyncStorage.getItem).toHaveBeenCalledWith('userToken');
      return getPromise.then(() => {
        expect(instance.props.navigation.navigate).toHaveBeenCalledWith('App');
      });
    });
  });

  describe('when storage has no userToken', () => {
    beforeEach(() => {
      getPromise = new Promise((resolve) => { resolve({}); });
      AsyncStorage.getItem = jest.fn().mockReturnValueOnce(
        getPromise,
      );
      wrapper = shallow(<AuthLoadingScreen navigation={navigation} />);
      instance = wrapper.instance() as AuthLoadingScreen;
    });

    it('navigates to Auth screen', () => {
      expect(AsyncStorage.getItem).toHaveBeenCalledWith('userToken');
      return getPromise.then(() => {
        expect(instance.props.navigation.navigate).toHaveBeenCalledWith('Auth');
      });
    });
  });

  describe('.render()', () => {
    it('renders a View', () => {
      expect(wrapper.find(View)).toBeDefined();
    });

    it('renders an ActivityIndicator', () => {
      expect(wrapper.find(ActivityIndicator)).toBeDefined();
    });

    it('renders a StatusBar with default barStyle', () => {
      const bar = wrapper.find(StatusBar);
      expect(bar).toBeDefined();
      expect(bar.props().barStyle).toBe('default');
    });
  });
});
