import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Deck } from '../../classes/Deck/Deck';
import { AddButton } from '../AddButton/AddButton.component';
import { List } from '../List/List.component';
import { styles } from './HomeScreen.styles';

export class HomeScreen extends Component {
  public render() {
    return (
      <View style={styles.root}>
        <Text style={styles.header}>Decks</Text>
        <List items={[
          new Deck('A Test Deck', []),
        ]}/>
        <AddButton/>
      </View>
    );
  }
}
