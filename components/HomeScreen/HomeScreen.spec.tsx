import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Text, View } from 'react-native';
import { AddButton } from '../AddButton/AddButton.component';
import { List } from '../List/List.component';
import { HomeScreen } from './HomeScreen.component';

describe('HomeScreen', () => {
  describe('.render()', () => {
    let described: HomeScreen;
    let wrapper: ShallowWrapper;

    beforeEach(() => {
      wrapper = shallow(
        <HomeScreen />);
      described = wrapper.instance() as HomeScreen;
    });

    it('renders a header', () => {
      const header = wrapper.find(Text);
      expect(header).toBeDefined();
      expect(header.render().text()).toBe('Decks');
    });

    it('renders a List component', () => {
      expect(wrapper.find(List)).toBeDefined();
    });

    it('renders an add button component', () => {
      expect(wrapper.find(AddButton)).toBeDefined();
    });
  });
});
