
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { ListItem } from '../ListItem/ListItem.component';

interface Props {
  items: ReadonlyArray<any>;
}

export class List extends Component<Props> {
  public static getName(item: any) {
    if (item.hasOwnProperty('name')) {
      return item.name;
    }
    if (item.hasOwnProperty('title')) {
      return item.title;
    }
    throw new TypeError(`Unable to exctract name for type ${typeof item}`);
  }

  public render() {
    return (
      <FlatList
        data={this.props.items}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
      />
    );
  }

  public renderItem({ item }: any) {
    return <ListItem name={List.getName(item)} />;
  }

  public keyExtractor(item: any, index: number) {
    return List.getName(item) + index;
  }
}
