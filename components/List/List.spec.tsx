import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { FlatList } from 'react-native';
import { ListItem } from '../ListItem/ListItem.component';
import { List } from './List.component';

describe('List', () => {
  let described: List;
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<List items={['A Deck', 'Another Deck']} />);
    described = wrapper.instance() as List;
  });

  describe('.render', () => {
    it('contains a FlatList', () => {
      expect(wrapper.find(FlatList)).toBeDefined();
    });
  });

  describe('.renderItem', () => {
    it('renders a ListItem', () => {
      const context = { item: {name: 'A deck'}};
      expect(described.renderItem(context)).toEqual(<ListItem name='A deck'/>);
    });
  });

  describe('.keyExtractor', () => {
    it('returns the given item name and index as the key', () => {
      expect(described.keyExtractor({name: 'foo'}, 1)).toEqual('foo' + 1);
    });
  });

  describe('getName', () => {
    it('returns object name, when object has name', () => {
      expect(List.getName({name: 'foo'})).toBe('foo');
    });

    it('returns object title, when object has title', () => {
      expect(List.getName({ title: 'foo' })).toBe('foo');
    });

    it('raise error, when object has no name or title', () => {
      expect(() => {
        List.getName({ key: 'foo' });
      }).toThrow(TypeError);
    });
  });
});
