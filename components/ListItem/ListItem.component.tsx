import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './ListItem.styles';

interface Props {
  name: string;
}

export class ListItem extends Component<Props> {
  public render() {
    return (
      <TouchableOpacity style={styles.root} activeOpacity={0.5}>
        <Text style={styles.deckName} numberOfLines={1}>
          {this.props.name}
        </Text>
        <TouchableOpacity>
          <Ionicons style={{...styles.icon, ...styles.edit}} name='md-create' color='white' size={30} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Ionicons style={{...styles.icon, ...styles.delete}} name='md-trash' color='white' size={30} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}
