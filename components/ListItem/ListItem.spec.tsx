import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Text } from 'react-native';
import { ListItem } from './ListItem.component';

describe('ListItem', () => {
  let described: ListItem;
  let wrapper: ShallowWrapper;
  let deckTitle: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<ListItem name='A Test Deck'/>);
    described = wrapper.instance() as ListItem;
    deckTitle = wrapper.find(Text);
  });

  describe('.render', () => {
    it('displays the given deck name', () => {
      expect(deckTitle.render().text()).toBe('A Test Deck');
    });
  });
});
