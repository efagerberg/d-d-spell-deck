import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    backgroundColor: 'gray',
    marginBottom: 5,
    flexDirection: 'row',
  },
  deckName: {
    fontSize: 20,
    padding: 10,
    color: 'white',
    flex: 1,
  },
  icon: {
    alignContent: 'center',
    textAlign: 'center',
    padding: 10,
    margin: 1,
  },
  edit: {
    backgroundColor: 'steelblue',
  },
  delete: {
    backgroundColor: 'tomato',
  },
});
