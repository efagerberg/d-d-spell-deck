import React from 'react';
import { Text, View } from 'react-native';
import { styles } from '../SettingsScreen/SettingsScreen.styles';

export class SettingsScreen extends React.Component {
  public render() {
    return (
      <View style={styles.root}>
        <Text style={{ fontSize: 100 }}>Settings</Text>
      </View>
    );
  }
}
