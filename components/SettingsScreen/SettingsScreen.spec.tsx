import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Text, View } from 'react-native';
import { SettingsScreen } from './SettingsScreen.component';

describe('SettingsScreen', () => {
  describe('.render()', () => {
    let described: SettingsScreen;
    let wrapper: ShallowWrapper;

    beforeEach(() => {
      wrapper = shallow(
        <SettingsScreen />);
      described = wrapper.instance() as SettingsScreen;
    });

    it('renders a a simple text view', () => {
      expect(wrapper.find(View));
      expect(wrapper.find(Text));
    });
  });
});
