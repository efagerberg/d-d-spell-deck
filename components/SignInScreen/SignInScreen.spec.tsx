import { shallow, ShallowWrapper } from 'enzyme';

import React from 'react';
import { SignInScreen } from './SignInScreen.component';

describe('StartScreen', () => {
  let wrapper: ShallowWrapper;
  let instance: SignInScreen;

  beforeEach(() => {
    wrapper = shallow(<SignInScreen/>);
    instance = wrapper.instance() as SignInScreen;
  });

  describe('.render()', () => {
    it('renders the component', () => {
      expect(wrapper).toBeDefined();
    });
  });
});
