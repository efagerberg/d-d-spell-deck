import { shallow, ShallowWrapper } from 'enzyme';

import React from 'react';
import { SignUpScreen } from './SignUpScreen.component';

describe('SignUpScreen', () => {
  let wrapper: ShallowWrapper;
  let instance: SignUpScreen;

  beforeEach(() => {
    wrapper = shallow(<SignUpScreen/>);
    instance = wrapper.instance() as SignUpScreen;
  });

  describe('.render()', () => {
    it('renders the component', () => {
      expect(wrapper).toBeDefined();
    });
  });
});
