import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { styles } from './CardDescription.styles';

export interface Props {
  description: string;
  atHigherLevelsDescription?: string;
}

export class CardDescription extends Component<Props> {
  public render() {
    return (
      <View style={styles.root}>
        <ScrollView style={this.descriptionStyle}>
          <Text style={styles.descriptionContent}>{this.props.description}</Text>
        </ScrollView>
        {this.atHigherLevelsSection}
      </View>
    );
  }

  private get descriptionStyle() {
    return (
      this.props.atHigherLevelsDescription ?
        styles.description :
        styles.descriptionSolo
    );
  }

  private get atHigherLevelsSection(): JSX.Element | null {
    if (this.props.atHigherLevelsDescription) {
      return (
        <View>
          <Text style={styles.atHigherLevelsLabel}>At Higher Levels</Text>
          <ScrollView style={styles.atHigherLevelsDescription}>
            <Text style={styles.atHigherLevelsContent}>{this.props.atHigherLevelsDescription}</Text>
          </ScrollView>
        </View>
      );
    }
    return null;
  }
}
