import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { CardDescription } from '../CardDescription/CardDescription.component';
import { styles } from './CardDescription.styles';

describe('CardDescription', () => {
  let fixture: ShallowWrapper;
  let described: CardDescription;

  beforeEach(() => {
    fixture = shallow(<CardDescription description='Foo'/>);
    described = fixture.instance() as CardDescription;
  });

  describe('render()', () => {
    describe('when the component is not given a atHigherLevelsDescription', () => {

      it('does not render the At Higher Levels Section', () => {
        expect(described).not.toContain(
          <Text style={styles.atHigherLevelsLabel}>At Higher Levels</Text>,
        );
      });
    });

    describe('when the component is given a atHigherLevelsDescription', () => {
      beforeEach(() => {
        fixture = shallow(<CardDescription description='Foo' atHigherLevelsDescription='Bar'></CardDescription>);
        described = fixture.instance() as CardDescription;
      });

      it('renders At Higher Levels Description and Prefix', () => {
        const expected = (
          <View>
            <Text style={styles.atHigherLevelsLabel}>At Higher Levels</Text>
            <ScrollView style={styles.atHigherLevelsDescription}>
              <Text>Bar</Text>
            </ScrollView>
          </View>
        );
        expect(fixture.contains(expected)).toBe(true);
      });
    });
  });
});
