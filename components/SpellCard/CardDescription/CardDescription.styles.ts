import EStyleSheet from 'react-native-extended-stylesheet';
import { media } from '../../../utils/media';

export const styles = EStyleSheet.create({
  root: {
    flex: 2,
  },
  description: {
    backgroundColor: 'white',
    padding: 5,
    fontSize: 12,
  },
  descriptionSolo: {
    backgroundColor: 'white',
    padding: 5,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  atHigherLevelsLabel: {
    textAlign: 'center',
    color: 'white',
    fontSize: 12,
    [media.tabletPortrait]: {
      fontSize: 20,
    },
  },
  atHigherLevelsDescription: {
    padding: 5,
    backgroundColor: 'white',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  descriptionContent: {
    [media.tabletPortrait]: {
      fontSize: 20,
    },
  },
  atHigherLevelsContent: {
    [media.tabletPortrait]: {
      fontSize: 20,
    },
  },
});
