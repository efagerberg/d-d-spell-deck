import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { styles } from './CardTextWindow.styles';

interface Props {
  color: string;
  castingTime: string;
  range: string;
  components: string;
  duration: string;
}

export class CardTextWindow extends Component<Props> {
  public render() {
    return (
      <View style={styles.root}>
        {this.getPanel('Casting Time', this.props.castingTime)}
        {this.getPanel('Range', this.props.range)}
        {this.getPanel('Components', this.props.components)}
        {this.getPanel('Duration', this.props.duration)}
      </View>
    );
  }

  public getPanel(title: string, content: string) {
    return (
      <View testID={title} style={this.getPanelStyle(title)}>
        {this.getPanelTitle(title)}
        <Text style={styles.windowPaneContent}>{content}</Text>
      </View>
    );
  }

  public getPanelStyle(title: string) {
    const baseStyle = {
      ...styles.windowPane,
      borderColor: this.props.color,
    };
    switch (title) {
      case 'Casting Time': {
        return {
          ...baseStyle,
          borderBottomWidth: 1,
          borderRightWidth: 1,
        };
      }
      case 'Range': {
        return {
          ...baseStyle,
          borderBottomWidth: 1,
          borderLeftWidth: 1,
        };
      }
      case 'Components': {
        return {
          ...baseStyle,
          borderTopWidth: 1,
          borderRightWidth: 1,
        };
      }
      case 'Duration': {
        return {
          ...baseStyle,
          borderTopWidth: 1,
          borderLeftWidth: 1,
        };
      }
    }
  }

    public getPanelTitle(title: string) {
    return (
      <Text testID='titlePanel' style={{
              ...styles.windowPaneTitle,
              color: this.props.color}}>
        {title}
      </Text>
    );
  }
}
