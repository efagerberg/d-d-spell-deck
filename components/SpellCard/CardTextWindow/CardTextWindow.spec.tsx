import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { CardTextWindow } from './CardTextWindow.component';

describe('CardTextWindow', () => {
  let wrapper: ShallowWrapper;
  let described: CardTextWindow;
  const props = {
    color: 'red',
    castingTime: '1 action',
    range: '1 foot',
    components: 'V,S,M',
    duration: '1 minute',
  };

  beforeEach(() => {
    wrapper = shallow(
      <CardTextWindow {...props} />);
    described = wrapper.instance() as CardTextWindow;
  });

  describe('.render()', () => {
    [
      ['Casting Time', props.castingTime,
        { borderBottomWidth: 1, borderRightWidth: 1 }],
      ['Range', props.range,
        { borderBottomWidth: 1, borderLeftWidth: 1 }],
      ['Components', props.components,
        { borderTopWidth: 1, borderRightWidth: 1 }],
      ['Duration', props.duration,
        { borderTopWidth: 1, borderLeftWidth: 1 }],
    ].forEach((scenerio) => {
      const title = scenerio[0] as string;
      const content = scenerio[1] as string;
      const expectedStyles = Object.assign(
        { borderColor: 'red' },
        scenerio[2],
      );

      it(`renders the ${title} pane with the correct styles`, () => {
        const pane = wrapper.find({ testID: title });
        const styles = pane.prop('style');
        const paneText = pane.render().text();

        expect(paneText).toContain(title);
        expect(paneText).toContain(content);
        Object.entries(expectedStyles).forEach(([key, value]) => {
          expect(styles).toHaveProperty(key, value);
        });

        const titleComp = pane.find({ testID: 'titlePanel' });
        const titleStyle = titleComp.prop('style');
        expect(titleStyle).toHaveProperty('color', described.props.color);
      });
    });
  });
});
