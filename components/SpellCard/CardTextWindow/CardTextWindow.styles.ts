import EStyleSheet from 'react-native-extended-stylesheet';
import { media } from '../../../utils/media';

export const styles = EStyleSheet.create({
  root: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 15,
  },
  windowPane: {
    backgroundColor: 'white',
    width: '50%',
    height: '50%',
    alignItems: 'center',
  },
  windowPaneTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    [media.tabletPortrait]: {
      fontSize: 25,
    },
  },
  windowPaneContent: {
    fontSize: 12,
    [media.tabletPortrait]: {
      fontSize: 20,
    },
  },
});
