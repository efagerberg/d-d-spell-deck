import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { styles } from './CardTitle.styles';

interface Props {
  title: string;
  levelOrdinal: string;
  school: string;
}

export class CardTitle extends Component<Props> {
  public render() {
    return (
      <View style={styles.root}>
        <Text numberOfLines={1} style={styles.title}>{this.props.title}</Text>
        <Text style={styles.LvlAndSchoolLabel}>
          {this.props.levelOrdinal}-level {this.props.school}
        </Text>
      </View>
    );
  }
}
