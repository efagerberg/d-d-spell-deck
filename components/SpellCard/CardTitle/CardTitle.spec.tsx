import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { CardTitle } from './CardTitle.component';

describe('CardTitle', () => {
  let wrapper: ShallowWrapper;
  const cardProps = {
    title: 'A title',
    levelOrdinal: '1st',
    school: 'evocation',
  };

  beforeEach(() => {
    wrapper = shallow(<CardTitle {...cardProps} />);
  });

  describe('.render()', () => {
    it('renders a view with a title, school, and level info', () => {
      const renderedText = wrapper.render().text();
      expect(renderedText).toContain(cardProps.title);
      expect(renderedText).toContain(
        `${cardProps.levelOrdinal}-level ${cardProps.school}`,
      );
    });
  });
});
