import EStyleSheet from 'react-native-extended-stylesheet';
import { media } from '../../../utils/media';

export const styles = EStyleSheet.create({
  root: {},
  title: {
    backgroundColor: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 24,
    [media.tabletPortrait]: {
      fontSize: 30,
    },
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  LvlAndSchoolLabel: {
    textAlign: 'center',
    color: 'white',
    fontSize: 12,
    [media.tabletPortrait]: {
      fontSize: 24,
    },
  },
});
