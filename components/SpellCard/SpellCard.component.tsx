import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Card } from '../../classes/Card/Card';
import { CardDescription } from './CardDescription/CardDescription.component';
import { CardTextWindow } from './CardTextWindow/CardTextWindow.component';
import { CardTitle } from './CardTitle/CardTitle.component';
import { styles } from './SpellCard.styles';

export class SpellCard extends Component<Card> {
  public render() {
    return (
      <View style={{ ...styles.root, backgroundColor: this.colorForSeries }}>
        <CardTitle {...this.props} levelOrdinal={this.levelOrdinal} />
        <CardTextWindow {...this.props} color={this.colorForSeries} />
        <CardDescription {...this.props }/>
        <Text style={styles.seriesLabel}>{this.props.series}</Text>
      </View>
    );
  }

  private get levelOrdinal(): string {
    const lastDigit = this.props.level % 10;
    if (lastDigit === 1) { return `${this.props.level}st`; }
    if (lastDigit === 2) { return `${this.props.level}nd`; }
    if (lastDigit === 3) { return `${this.props.level}rd`; }
    return `${this.props.level}th`;
  }

  private get colorForSeries() {
    switch (this.props.series) {
      case 'Paladin': {
        return 'lightskyblue';
      }
      case 'Arcane': {
        return 'indianred';
      }
      case 'Cleric': {
        return 'goldenrod';
      }
      case 'Bard': {
        return 'purple';
      }
      case 'Druid': {
        return 'forestgreen';
      }
      case 'Ranger': {
        return 'saddlebrown';
      }
      case 'Barbarian': {
        return 'brown';
      }
      case 'Fighter': {
        return 'cadetblue';
      }
      case 'Monk': {
        return 'palegoldenrod';
      }
      default: {
        /* tslint:disable:no-bitwise */
        return '#' + ((1 << 24) * Math.random() | 0).toString(16);
      }
    }
  }
}
