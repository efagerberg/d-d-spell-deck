import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Card } from '../../classes/Card/Card';
import { CardDescription } from './CardDescription/CardDescription.component';
import { CardTextWindow } from './CardTextWindow/CardTextWindow.component';
import { CardTitle } from './CardTitle/CardTitle.component';
import { SpellCard } from './SpellCard.component';

const defaultCard = {
  title: 'Test Spell',
  level: 1,
  school: 'evocation',
  castingTime: '1 action',
  duration: '1 minute',
  range: '40 feet',
  components: 'V,S,M',
  description: 'This spell gives you one free ice cream sandwich',
  series: 'Monk',
} as Card;

describe('Card', () => {
  let wrapper: ShallowWrapper;
  let described: SpellCard;

  beforeEach(() => {
    wrapper = shallow(<SpellCard {...defaultCard} />);
    described = wrapper.instance() as SpellCard;
  });

  describe('.render()', () => {
    it('renders a CardTitle', () => {
      const cardTitle = wrapper.find(CardTitle);
      expect(cardTitle).toHaveLength(1);
    });

    describe('rendering the expected ordinal levels in the title', () => {
      [
        [1, '1st'], [2, '2nd'], [3, '3rd'], [4, '4th'], [5, '5th'],
        [6, '6th'], [7, '7th'], [8, '8th'], [9, '9th'], [10, '10th'],
        [102345, '102345th'], [1023431, '1023431st'], [82, '82nd'],
      ].forEach((scenerio) => {
        const level = scenerio[0] as number;
        const expected = scenerio[1] as string;

        it(`renders level ${level} ordinal as ${expected}`, () => {
          const card = { ...defaultCard, level };
          wrapper = shallow(<SpellCard {...card} />);
          const cardTitle = wrapper.find(CardTitle);
          expect(cardTitle.prop('levelOrdinal')).toBe(expected);
        });
      });
    });

    it('renders a CardDescription', () => {
      expect(wrapper.contains(<CardDescription {...described.props} />)).toBe(true);
    });

    it('renders a CardTextWindow', () => {
      const window = wrapper.find(CardTextWindow);
      expect(window).toHaveLength(1);
    });

    describe('series derived coloring', () => {
      beforeEach(() => {
        spyOn(Math, 'random').and.returnValue(0.1);
      });

      [
        ['Paladin', 'lightskyblue'],
        ['Arcane', 'indianred'],
        ['Cleric', 'goldenrod'],
        ['Bard', 'purple'],
        ['Druid', 'forestgreen'],
        ['Barbarian', 'brown'],
        ['Ranger', 'saddlebrown'],
        ['Fighter', 'cadetblue'],
        ['Monk', 'palegoldenrod'],
        ['foobar', '#199999'],
      ].forEach((scenerio) => {
        const series = scenerio[0] as string;
        const expectedColor = scenerio[1] as string;

        it(`sets color ${expectedColor} when given series ${series}`, () => {
          const card = { ...defaultCard, series };
          wrapper = shallow(<SpellCard {...card} />);
          const expected = (
            <CardTextWindow {...card} color={expectedColor} />
          );

          expect(wrapper.contains(expected)).toBe(true);
        });
      });
    });
  });
});
