import EStyleSheet from 'react-native-extended-stylesheet';
import { media } from '../../utils/media';

// This component should always be roughly the same size for phones so no flexing
export const styles = EStyleSheet.create({
  root: {
    borderRadius: 15,
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
  },
  seriesLabel: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    [media.tabletPortrait]: {
      fontSize: 30,
    },
    paddingBottom: 5,
  },
});
