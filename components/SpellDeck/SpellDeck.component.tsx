import React, { Component, Fragment } from 'react';
import {
  Animated, Dimensions, GestureResponderEvent, PanResponder,
  PanResponderGestureState, PanResponderInstance,
} from 'react-native';
import { Card } from '../../classes/Card/Card';
import { Deck } from '../../classes/Deck/Deck';
import { SpellCard } from '../SpellCard/SpellCard.component';
import { styles } from './SpellDeck.styles';

export interface Props {
  deck: Deck;
}

interface State {
  currentCard: Card;
  nextCard: Card | null;
}

const SCREEN_WIDTH = Dimensions.get('window').width;

export class SpellDeck extends Component<Props, State> {
  private panResponder: PanResponderInstance;
  private position: Animated.ValueXY;
  private swipeThreshold: number = 60;
  private xDistancePadding: number = 60;

  constructor(props: Props) {
    super(props);
    this.position = new Animated.ValueXY();
    this.state = {
      currentCard: this.props.deck.first,
      nextCard: this.props.deck.second,
    };

    /* istanbul ignore next */
    // https://github.com/gotwarlost/istanbul/issues/486
    this.panResponder = PanResponder.create({
      onPanResponderEnd: (evt, gestureState) =>
        this.swipeEnd(evt, gestureState),
      onPanResponderMove: (evt, gestureState) =>
        this.swipeStart(evt, gestureState),
      onStartShouldSetPanResponder: (evt, gestureState) => true,
    });
  }

  public swipeStart(evt: GestureResponderEvent, gestureState: PanResponderGestureState) {
    this.prepareNextCard(gestureState);
    this.position.setValue({ x: gestureState.dx, y: 0 });
  }

  public swipeEnd(evt: GestureResponderEvent, gestureState: PanResponderGestureState) {
    if (gestureState.dx < -this.swipeThreshold) {
      this.swipeLeft(gestureState);
    } else if (gestureState.dx > this.swipeThreshold) {
      this.swipeRight(gestureState);
    } else {
      Animated.spring(this.position, {
        friction: 10,
        toValue: { x: 0, y: 0 },
      }).start();
    }
  }

  public render() {
    const animatedViews = this.props.deck.map((card, i) => {
      const isCurrent = card === this.state.currentCard;
      const isNext = card === this.state.nextCard;
      if (isCurrent && this.props.deck.length > 1) {
        return (
          <Animated.View
            key={card.title}
            style={[this.position.getTranslateTransform(),
            { ...styles.animatedView }]}
            {...this.panResponder.panHandlers}>
            <SpellCard {...card} />
          </Animated.View>
        );
      }
      if (isNext || (this.props.deck.length === 1 && isCurrent)) {
        return (
          <Animated.View style={styles.animatedView} key={card.title}>
            <SpellCard {...card} />
          </Animated.View>
        );
      }
    }).reverse();
    return (
      <Fragment>
        {animatedViews}
      </Fragment>
    );
  }

  private prepareNextCard(gestureState: PanResponderGestureState) {
    // The next card should be set whenever the user starts to swipe
    // left or right.
    if (gestureState.dx <= 0) {
      this.setState({ nextCard: this.props.deck.second });
    }
    if (gestureState.dx > 0) {
      this.setState({ nextCard: this.props.deck.last });
    }
  }

  private swipeLeft(gestureState: PanResponderGestureState) {
    Animated.timing(this.position, {
      duration: 100,
      toValue: { x: -SCREEN_WIDTH - this.xDistancePadding, y: gestureState.dy },
    }).start(() => {
      this.setState({ currentCard: this.props.deck.next() }, () => {
        this.position.setValue({ x: 0, y: 0 });
      });
    });
  }

  private swipeRight(gestureState: PanResponderGestureState) {
    Animated.timing(this.position, {
      duration: 100,
      toValue: { x: SCREEN_WIDTH + this.xDistancePadding, y: gestureState.dy },
    }).start(() => {
      this.setState({ currentCard: this.props.deck.previous() }, () => {
        this.position.setValue({ x: 0, y: 0 });
      });
    });
  }
}
