import { shallow, ShallowWrapper } from 'enzyme';
import React, { Fragment } from 'react';
import { Animated, GestureResponderEvent, PanResponderGestureState } from 'react-native';
import { generateCard } from '../../classes/Card/Card.spec.helper';
import { Deck } from '../../classes/Deck/Deck';
import { SpellDeck } from './SpellDeck.component';

describe('SpellDeck', () => {
  let wrapper: ShallowWrapper;
  let instance: SpellDeck;
  let deck: Deck;

  beforeEach(() => {
    deck = new Deck(
      'A Test Deck',
      [
        generateCard({ title: 'Spell 1' }),
        generateCard({ title: 'Spell 2' }),
        generateCard({ title: 'Spell 3' }),
      ],
    );
    // Do not use deck that is used for assertion in comp
    const compDeck = new Deck('A Test Deck', deck);
    wrapper = shallow(<SpellDeck deck={compDeck} />);
    instance = wrapper.instance() as SpellDeck;
  });

  describe('constructor', () => {
    it('sets the current card to the deck\'s current card', () => {
      expect(instance.state.currentCard).toBe(deck.first);
    });
  });

  describe('panResponder', () => {
    let gestureState: PanResponderGestureState;
    const evt: GestureResponderEvent = {} as GestureResponderEvent;

    describe('.swipeStart()', () => {
      describe('when the gestureState in', () => {
        beforeEach(() => {
          gestureState = { dx: -1 } as PanResponderGestureState;
          expect(instance.state.nextCard).toBe(deck.second);
        });

        it('sets the next card to the deck\'s second card', () => {
          instance.swipeStart(evt, gestureState);
        });
      });

      describe('when the user starts to swipe right', () => {
        beforeEach(() => {
          gestureState = { dx: 1 } as PanResponderGestureState;
          instance.swipeStart(evt, gestureState);
        });

        it('sets the next card to the deck\'s last card', () => {
          expect(instance.state.nextCard).toBe(deck.last);
        });
      });

      describe('when the user does not swipe left or right', () => {
        beforeEach(() => {
          gestureState = { dx: 0 } as PanResponderGestureState;
          instance.swipeStart(evt, gestureState);
        });

        it('sets the next card to the deck\'s second card', () => {
          expect(instance.state.currentCard).toBe(deck.first);
        });
      });
    });

    describe('.swipeEnd()', () => {
      describe('when the user has swiped far enough to the left', () => {
        beforeEach(() => {
          gestureState = { dx: -100 } as PanResponderGestureState;
          instance.swipeEnd(evt, gestureState);
        });

        it('sets the current card to the deck\'s second card', () => {
          expect(instance.state.currentCard).toBe(deck.second);
        });

        it('calls Animated.timing', () => {
          expect(Animated.timing).toBeCalled();
        });
      });

      describe('when the user has swiped far enough to the right', () => {
        beforeEach(() => {
          gestureState = { dx: 100 } as PanResponderGestureState;
          instance.swipeEnd(evt, gestureState);
        });

        it('sets the current card to the deck\'s last card', () => {
          expect(instance.state.currentCard).toBe(deck.last);
        });

        it('calls Animated.timing', () => {
          expect(Animated.timing).toBeCalled();
        });
      });

      describe('when the user has not swiped far enough', () => {
        beforeEach(() => {
          gestureState = { dx: 1 } as PanResponderGestureState;
          instance.swipeEnd(evt, gestureState);
        });

        it('still has the current card set to the deck\'s first card', () => {
          expect(instance.state.currentCard).toBe(deck.first);
        });

        it('calls Animated.spring', () => {
          expect(Animated.timing).toBeCalled();
        });
      });
    });
  });

  describe('.render()', () => {
    const panHandlerNames = [
      'onStartShouldSetResponder',
      'onResponderMove',
      'onResponderEnd',
    ];
    let view: ShallowWrapper;

    function expectAnimatedViewHasPanHandlers(v: ShallowWrapper) {
      expect(v).toBeDefined();
      expect(v.length).toBe(1);
      panHandlerNames.forEach((handler) => {
        expect(v.props()).toHaveProperty(handler);
      });
    }

    function expectAnimatedViewHasNoPanHandlers(v: ShallowWrapper) {
      expect(v).toBeDefined();
      expect(v.length).toBe(1);
      panHandlerNames.forEach((handler) => {
        expect(v.props()).not.toHaveProperty(handler);
      });
    }

    function expectNoChildren(w: ShallowWrapper) {
      const fragment = w.find('Fragment');
      expect(fragment).toBeDefined();
      expect(fragment.children().length).toBe(0);
    }

    describe('when the deck no cards', () => {
      beforeEach(() => {
        wrapper.setProps({ deck: new Deck('A Test Deck', []) });
      });

      it('does not render any children under the root', () => {
        expectNoChildren(wrapper);
      });
    });

    describe('when the deck contains the component\'s current and next card', () => {
      beforeEach(() => {
        view = wrapper.find(Animated.View);
        expect(view.length).toBe(2);
      });

      it('renders an animated view with pan handlers on top', () => {
        expectAnimatedViewHasPanHandlers(view.at(1));
      });

      it('renders an animated view without pan handlers on bottom', () => {
        expectAnimatedViewHasNoPanHandlers(view.at(0));
      });
    });

    describe('when the deck contains only the component\'s current card', () => {
      beforeEach(() => {
        deck = new Deck('A Test Deck', [generateCard()]);
        wrapper.setProps({ deck });
        wrapper.setState({ currentCard: deck.first });
        view = wrapper.find(Animated.View);
      });

      it('renders an animated view with no pan handlers', () => {
        expectAnimatedViewHasNoPanHandlers(view);
      });
    });
  });
});
