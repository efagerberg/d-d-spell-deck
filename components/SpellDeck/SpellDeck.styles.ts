import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  animatedView: {
    height: '100%',
    position: 'absolute',
    width: '100%',
  },
});
