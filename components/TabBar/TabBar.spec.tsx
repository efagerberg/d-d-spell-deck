import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { getTabIcon, TabBar } from './TabBar';

describe('TabBar', () => {
  it('should be defined', () => {
    expect(TabBar).toBeDefined();
  });
});

describe('getTabIcon', () => {
  it('returns a function that returns an icon', () => {
    const name = 'md-home';
    const color = 'red';
    expect(getTabIcon(name)({tintColor: color})).toEqual(
      <Ionicons name={name} color={color} size={30} />,
    );
  });
});
