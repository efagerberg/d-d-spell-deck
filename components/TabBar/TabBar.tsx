import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { createBottomTabNavigator, NavigationRouteConfigMap } from 'react-navigation';
import { HomeScreen } from '../HomeScreen/HomeScreen.component';
import { SettingsScreen } from '../SettingsScreen/SettingsScreen.component';

export function getTabIcon(name: string) {
  return ({ tintColor }: NavigationRouteConfigMap) => (
    <Ionicons name={name} color={tintColor} size={30} />
  );
}

export const TabBar = createBottomTabNavigator(
  {
    Home: {
      navigationOptions: {
        tabBarIcon: getTabIcon('md-home'),
      },
      screen: HomeScreen,
    },
    Settings: {
      navigationOptions: {
        tabBarIcon: getTabIcon('md-settings'),
      },
      screen: SettingsScreen,
    },
  },
  {
    tabBarOptions: {
      activeBackgroundColor: 'tomato',
      inactiveBackgroundColor: 'gray',
      activeTintColor: 'black',
      inactiveTintColor: 'black',
      showLabel: false,
    },
  },
);
