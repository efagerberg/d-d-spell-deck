module.exports = {
  preset: 'react-native',
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js'
  ],
  transform: {
    '\\.(ts|tsx)$': 'ts-jest',
    '^.+\\.js$': './node_modules/react-native/jest/preprocessor.js',
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  testPathIgnorePatterns: [
    '\\.snap$',
    './node_modules/',
  ],
  transformIgnorePatterns: [
    'node_modules/(?!(jest-)?react-native|react-navigation)'
  ],
  testEnvironment: 'node',
  cacheDirectory: '.jest/cache',
  setupTestFrameworkScriptFile: '<rootDir>/setUpTests.js',
  coverageThreshold: {
    'global': {
      'branches': 100,
      'functions': 100,
      'lines': 100,
      'statements': 100
    }
  },
  collectCoverageFrom: [
    'components/**.{ts,tsx}',
  ],
  collectCoverage: true
}
