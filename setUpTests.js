import { Animated } from 'react-native';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

const originalConsoleError = console.error;
console.error = (message) => {
  // see: https://jestjs.io/docs/en/tutorial-react.html#snapshot-testing-with-mocks-enzyme-and-react-16
  // see https://github.com/Root-App/react-native-mock-render/issues/6
  if (message.startsWith('Warning:')) {
    return;
  }

  originalConsoleError(message);
};

Enzyme.configure({ adapter: new Adapter() });

jest.mock('Animated', () => {
  return {
    ValueXY: jest.fn(() => {
      return {
        getTranslateTransform: jest.fn(),
        setValue: jest.fn(),
      };
    }),
    View: jest.fn(() => {
      return Animated.View;
    }),
    spring: jest.fn(() => ({
      start: (callback) => callback && callback(),
    })),
    timing: jest.fn(() => ({
      start: (callback) => callback && callback(),
    })),
    createAnimatedComponent: jest.fn(),
  };
});

jest.mock('@expo/vector-icons');
