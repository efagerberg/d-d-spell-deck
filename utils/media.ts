export const media = {
  phoneLandscape: '@media (min-width: 481) and (max-width: 767)',
  phonePortrait: '@media (min-width: 320) and (max-width: 480)',
  tabletLanscape: '@media (min-width: 768) and (max-width: 1024) and (orientation: landscape)',
  tabletPortrait: '@media (min-width: 768) and (max-width: 1024)',
};
